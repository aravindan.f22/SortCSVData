package models;

public class InsertedProduct {
    private int count;
    private int maxPosition;

    public InsertedProduct(int count, int maxPosition) {
        this.count = count;
        this.maxPosition = maxPosition;
    }
    public InsertedProduct() {}
    public void setCount(int count) {
        this.count = count;
    }

    public void setMaxPosition(int maxPosition) {
        this.maxPosition = maxPosition;
    }

    public int getCount() {
        return count;
    }

    public int getMaxPosition() {
        return maxPosition;
    }
}
