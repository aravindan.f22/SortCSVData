package models;

public class Product {

    public Product(String[] rowItems) {
        this.name = rowItems[0];
        this.condition = rowItems[1];
        this.state = rowItems[2];
        this.productId = Integer.parseInt(rowItems[3]);
        this.price = Float.parseFloat(rowItems[4]);
        this.uniqueId = rowItems[5];
    }

    private String name;
    private String condition;
    private String state;
    private String uniqueId;
    private int productId;
    private double price;

    public Product(String name, String condition, String state, int productId, double price, String uniqueId) {
        this.name = name;
        this.condition = condition;
        this.state = state;
        this.productId = productId;
        this.price = price;
        this.uniqueId = uniqueId;
    }

    public int getProductId() { return productId; }

    public double getPrice() {
        return price;
    }

    public String getName() {
        return name;
    }

    public String getCondition() {
        return condition;
    }

    public String getState() {
        return state;
    }

    public String getUniqueId() {
        return uniqueId;
    }
}