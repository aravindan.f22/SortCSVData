package data;

import interfaces.SortCallback;
import models.Product;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class CSVParser implements Runnable {

    private final String filePath;
    private final SortCallback sortCallback;

    public CSVParser(String filePath, SortCallback sortCallback) {
        this.filePath = filePath;
        this.sortCallback = sortCallback;
    }

    @Override
    public void run() {
        try {
            File file = new File(filePath);
            FileReader inputFS = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(inputFS);
            String line = null;

            while ((line = bufferedReader.readLine()) != null && !line.isEmpty()) {
                String[] fields = line.split(",");
                Product sourceData = new Product(fields);
                sortCallback.onDataReceived(sourceData);
            }
            bufferedReader.close();
        } catch (IOException e) { e.printStackTrace(); }
    }
}