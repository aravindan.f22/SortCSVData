package thread;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ExecutorHandler {
    private static ExecutorHandler instance;
    final int THREAD_COUNT = 3;

    private ExecutorHandler() {
        executorService = Executors.newFixedThreadPool(THREAD_COUNT);
    }

    private ExecutorService executorService;

    public static synchronized ExecutorHandler getInstance() {
        if (instance == null) {
            instance = new ExecutorHandler();
        }
        return instance;
    }

    public ExecutorService getExecutorService() {
        return executorService;
    }

    public void shutdown() {
        executorService.shutdown();
        instance = null;
    }
}
