import com.opencsv.CSVWriter;
import data.CSVParser;
import interfaces.SortCallback;
import models.InsertedProduct;
import models.Product;
import thread.ExecutorHandler;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.TimeUnit;

public class SourceDataHandler implements SortCallback {
    private final String DIRECTORY_PATH = "/Users/aravindan/Desktop/files/";
    private final List<Product> productArray = new ArrayList<>();
    private final Hashtable insertedProducts = new Hashtable<String, InsertedProduct>();
    private Product highestValuedProduct;

    static final int MAX_PRODUCTS = 20;
    static final int TOTAL_LENGTH = 1000;

    void processSourceData() {
        parseCSVFiles();
    }

    private void parseCSVFiles() {
        File directory = new File(DIRECTORY_PATH);
        if (!directory.exists()) return;
        for (final File file : Objects.requireNonNull(directory.listFiles())) {
            if (!file.isDirectory() && file.getName().contains("csv")) {
                CSVParser csvParser = new CSVParser(file.getPath(), this);
                ExecutorHandler.getInstance().getExecutorService().execute(csvParser);
            }
        }
        shutdownExecutorAndProcessData();
    }

    private void shutdownExecutorAndProcessData() {
        ExecutorHandler executor = ExecutorHandler.getInstance();
        executor.shutdown();
        System.out.println("shutdown");
        try {
            while (!executor.getExecutorService().awaitTermination(Long.MAX_VALUE, TimeUnit.SECONDS)) {
                System.out.println("waiting for executor to end");
            }
            processFinalData();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void processFinalData() {
        productArray.sort(Comparator.comparingDouble(Product::getPrice));
        System.out.println(productArray.size());
        try {
            File file = new File(DIRECTORY_PATH + "output/output.csv");
            file.getParentFile().mkdirs();
            FileWriter outputFile = new FileWriter(file);
            CSVWriter writer = new CSVWriter(outputFile);

            try {
                for (Product product : productArray) {
                    String[] productArray = {product.getName(), String.valueOf(product.getProductId()), String.valueOf(product.getPrice()),
                    product.getState(), product.getCondition()};
                    writer.writeNext(productArray);
                }

            } catch (Exception e) { e.printStackTrace(); }

            writer.close();
        }
        catch (IOException e) { e.printStackTrace(); }
    }


    @Override
    public synchronized void onDataReceived(Product product) {
        if (isValidInsertion(product)) {
            insertRecord(product);
            updateHighestValuedProduct(product);
        }
    }

    private synchronized void insertRecord(Product product) {
        productArray.add(product);
    }

    private synchronized void removeRecord(Product product) {
        boolean delete = productArray.remove(product);
//        if (delete)
//            System.out.println(product.getUniqueId() + " - " + product.getProductId() + " : Remove Product - success");
    }

    private synchronized void updateHighestValuedProduct(Product product) {
        highestValuedProduct = productArray.stream().max(Comparator.comparingDouble(Product::getPrice)).get();

        Object insertedProductString = insertedProducts.get(product.getProductId());
        InsertedProduct insertedProduct;

        if (insertedProductString == null) {
            insertedProduct = new InsertedProduct();
        } else {
            insertedProduct = (InsertedProduct) insertedProductString;
        }

        Product maxiProduct = productArray.stream().filter(product1 -> product1.getProductId() == product.getProductId()).max(Comparator.comparingDouble(Product::getPrice)).get();

        insertedProduct.setCount((int)productArray.stream().filter(product1 -> product1.getProductId() == product.getProductId()).count());
        insertedProduct.setMaxPosition(productArray.indexOf(maxiProduct));
        insertedProducts.put(product.getProductId(), insertedProduct);
//        Product maxProduct = productArray.get(((InsertedProduct) insertedProducts.get(product.getProductId())).getMaxPosition());
//        System.out.println(maxProduct.getUniqueId() + " - " + maxProduct.getProductId() + " : Max Product");
    }

    private synchronized boolean isValidInsertion(Product product) {

        if (productArray.size() < TOTAL_LENGTH) return true; // first element
        if (product.getPrice() >= highestValuedProduct.getPrice()) return false;

        InsertedProduct insertedProduct = (InsertedProduct) insertedProducts.get(product.getProductId());
        if (insertedProduct == null) {
            removeRecord(highestValuedProduct);
            return true;
        }

        Product highestProduct = productArray.get(insertedProduct.getMaxPosition());

        if (insertedProduct.getCount() >= MAX_PRODUCTS && (product.getPrice() < highestProduct.getPrice())) {
            removeRecord(highestProduct);
            return true;
        } else if (insertedProduct.getCount() < MAX_PRODUCTS){
            removeRecord(highestValuedProduct);
            return true;
        }

        return false;
    }
}
