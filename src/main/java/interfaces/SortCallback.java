package interfaces;

import models.Product;

public interface SortCallback {
    default void onDataReceived(Product sourceData) {}
}
