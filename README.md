This project Parses multiple CSV files, validates the data based on the below conditions and finally exports to another CSV file.

1. Products must be sorted based on the price.
2. 1000 products with lowest price must be written to a CSV file.
3. The final list must not contain products with same id repeated more than 20 times.